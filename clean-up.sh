#!/bin/bash
rm backend-webapp.yaml
rm build-tag-push.sh
rm dockerfile-celery-beat
rm dockerfile-nginx
rm Dockerfile
rm docker_entrypoint.sh
rm intel_owl.ini
rm dockerfile-celery-worker
rm dockerfile-rabbitmq
rm frontend-deployment.yaml
rm kustomization.yaml
rm rabbitmq-daemon.yaml
rm services.yaml
rm deploy.sh
rm clean-up.sh