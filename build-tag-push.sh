#!/bin/bash
set -e;
echo "###############"
echo "Fetching Latest files from IntelOwl"
echo "###############"
cp docker/docker_entrypoint.sh .
cp configuration/intel_owl.ini .
cp docker/Dockerfile .
cp docker/Dockerfile_nginx ./dockerfile-nginx
echo "###############"
echo "Modifying files for K8S Deployment"
echo "###############"
{
echo "
RUN mkdir -p /etc/uwsgi/sites && mv intel_owl.ini /etc/uwsgi/sites/"
echo "COPY configuration/* /opt/deploy/configuration/"
} >> ./Dockerfile
{
echo "
COPY ./configuration/intel_owl_nginx_http /etc/nginx/conf.d/default.conf"
echo "EXPOSE 80 443"
} >> ./dockerfile-nginx
echo "###############"
echo "Building, Tagging, and Pushing Images to Docker Registry: $1"
echo "###############"
docker build --tag "$1"/intel-owl:latest .
docker push "$1"/intel-owl:latest
sed -i -e 's/server uwsgi/server backend-webapp-service.default.svc.cluster.local/g' ./configuration/intel_owl_nginx_http
sed -i -e "s/changeit/$1/g" ./dockerfile-celery-beat ./dockerfile-celery-worker ./dockerfile-nginx
docker build --tag "$1"/celery-beat:latest -f dockerfile-celery-beat .
docker push "$1"/celery-beat:latest
docker build --tag "$1"/celery-worker:latest -f dockerfile-celery-worker .
docker push "$1"/celery-worker:latest
docker build --tag "$1"/nginx-rp:latest -f ./dockerfile-nginx .
docker push "$1"/nginx-rp:latest
docker build --tag "$1"/rabbitmq:latest -f dockerfile-rabbitmq .
docker push "$1"/rabbitmq:latest
echo "###############"
echo "Updating Docker Repo in deployment files"
echo "###############"
sed -i -e "s/<docker-repo>/$1/g" ./backend-webapp.yaml ./frontend-deployment.yaml ./rabbitmq-daemon.yaml
